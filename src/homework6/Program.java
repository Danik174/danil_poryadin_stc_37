package homework6;

public class Program {
   public String nameProgram;

    /**
     *
     * @param nameProgram
     */
    public Program(String nameProgram) {
        this.nameProgram = nameProgram;
    }

    /**
     *
     * @return
     */
    public String getNameProgram() {
        return nameProgram;
    }

    public void setNameProgram(String nameProgram) {
        this.nameProgram = nameProgram;
    }
}
