package homework6;

import java.util.ArrayList;
import java.util.Random;

public class Channel {
    String nameChanel;
    Program[] program;

    public Channel(String nameChanel, Program[] program) {
        this.nameChanel = nameChanel;
        this.program = program;
    }
    public void turnOn(){
        Random random = new Random();
        int position = random.nextInt(program.length);
        String nameProgram = program[position].getNameProgram();
        System.out.println(nameProgram);
    }
}
