package homework6;

import java.util.Scanner;


/**
 * @author Danil Poryadin
 */
public class Main {

    public static void main(String[] args) {
        Program[] nemeProgramChanel1 = new Program[]{
                new Program("пусто1"),
                new Program("пусто2"),
                new Program("пусто3"),
                new Program("пусто4")
        };
        Program[] nemeProgramChanel2 = new Program[]{
                new Program("пусто5"),
                new Program("пусто6"),
                new Program("пусто7"),
                new Program("пусто8")

        };
        Program[] nemeProgramChanel3 = new Program[]{
                new Program("пусто9"),
                new Program("пусто10"),
                new Program("пусто11"),
                new Program("пусто12")

        };
        Program[] nemeProgramChanel4 = new Program[]{
                new Program("пусто13"),
                new Program("пусто14"),
                new Program("пусто15"),
                new Program("пусто16")
        };
        Channel[] channels = new Channel[]{
                new Channel("ОРТ",nemeProgramChanel1),
                new Channel("РТР", nemeProgramChanel2),
                new Channel("СТС",nemeProgramChanel3),
                new Channel("ТНТ",nemeProgramChanel4)
        };
        TV tv =new TV(channels);
        RemoteController remoteController = new RemoteController(tv);
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("введите номер программы или символ 10 для выхода");


            int i = scanner.nextInt();
            if (i == 10){
                break;
            }
            if (i == 9) {
                System.out.println("Введите название канала:");
                String NewNameOfChannel = scanner.next();
                remoteController.pressButton2(NewNameOfChannel);

            } else {
                remoteController.pressButton(i);
            }
        }

    }
}
