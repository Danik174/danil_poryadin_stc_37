package homework10;

public class Main {

    public static void main(String[] args) {
        // создал список
        InnoList list = new InnoLinkedList();

        list.add(7);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(15);
        list.add(20);
        list.add(-77);
        list.add(100);
        System.out.println("оПЕРАЦИЯ Get");

        System.out.println(list.get(0)); // 7
        System.out.println(list.get(3)); // 12
        System.out.println(list.get(7)); // 100

        InnoIterator iterator = list.iterator();
        System.out.println("Вывод значения при помощи итерратора");

        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }
        System.out.println(list.contains(20));
        list.remove(20);
        System.out.println(list.contains(20));

        InnoIterator iterator2 = list.iterator();
        System.out.println("Дублирующий итерратор");

        while (iterator2.hasNext()) {
            System.out.println(iterator2.next() + " ");
        }

    }
}
