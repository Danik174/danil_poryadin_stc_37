package homework8;

 public abstract class Shapes implements Movable, Scalable {
    protected int x;
    protected int y;
    public double side1;
    public double side2;

     public abstract double area();

     public abstract double perimeter();


     public Shapes(int x, int y, double side1, double side2) {
        this.x = x;
        this.y = y;
        this.side1 = side1;
        this.side2 = side2;
    }

     public int getX() {

         return x;
     }

     public void setX(int x) {

         this.x = x;
     }

     public int getY() {

         return y;
     }

     public void setY(int y) {

         this.y = y;
     }

     public double getSide1() {

         return side1;
     }

     public void setSide1(double side1) {

         this.side1 = side1;
     }

     public double getSide2() {

         return side2;
     }

     public void setSide2(double side2) {

         this.side2 = side2;
     }

     @Override
     public void moveCentr(int xMove, int yMove) {
         this.setX(this.getX() + xMove);
         this.setY(this.getY() + yMove);
     }

     @Override
     public void scale(int Number) {
         this.side1 = side1 * Number;
         this.side2 = side2 * Number;

     }



 }



