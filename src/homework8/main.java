package homework8;

public class main {
    public static void main(String[] args) {
        Shapes ellipse = new Ellipse(4.5,3.25);
        double l1 = ellipse.area();
        System.out.println("Площадь Эллипса = " + l1);

        System.out.println("Периметр эллипса =" + ellipse.perimeter());

        Shapes circle = new Circle(3);
        System.out.println("Площадь круга ="+ circle.area());
        System.out.println("Периметр круга =" + circle.perimeter());
        Shapes rectangle = new Rectangle(10,5);
        System.out.println("Площадь Прямоугольника =" + rectangle.area());
        System.out.println("Периметр Прямоугольника =" + rectangle.perimeter());
        Shapes square = new Square(5);
        System.out.println("Площадь Квадрата ="+square.area());

        System.out.println();
        System.out.println("X = 0;Y = 0");

        ellipse.moveCentr(3,2);
        System.out.println("Изменение координат Эллипса:"+"Х = "+ ellipse.getX()+ " "+"Y = " + ellipse.getY());
        circle.moveCentr(5,6);
        System.out.println("Изменение координат круга:"+"Х = "+ circle.getX()+ " "+"Y = " + circle.getY());
        rectangle.moveCentr(7,9);
        System.out.println("Изменение координат Прямоугольника:"+"Х = "+ rectangle.getX()+ " "+"Y = " + rectangle.getY());
        square.moveCentr(6,9);
        System.out.println("Изменение координат Квадрата:"+"Х = "+ square.getX()+ " "+"Y = " + square.getY());

        System.out.println();

        ellipse.scale(2);
        System.out.println("Площадь Эллипса после измения =" + ellipse.area());
        System.out.println("Периметр Эллипса после измения =" + ellipse.perimeter());
        circle.scale(3);
        System.out.println("Площадь Круга после измения =" + circle.area());
        System.out.println("Периметр круга после измения =" + circle.perimeter());
        rectangle.scale(4);
        System.out.println("Площадь Прямоугольника после измения =" + rectangle.area());
        System.out.println("Периметр Прямоугольника после измения =" + rectangle.perimeter());
        square.scale(5);
        System.out.println("Площадь Квадвата после измения =" + square.area());








    }
}
