package homework8;

public class Ellipse extends Shapes {
    final static double Pi = 3.1415926536;

    /**
     * Создаем фигуру эллипс
     * @param side1 параметр 1
     * @param side2 параметр 2
     */
    public Ellipse(double side1 , double side2 ){
        super (0,0, side1 , side2);



    }

    /**
     * создаем ссылку на площадь
     * @return задаем формулу площади для фигуры
     */
    @Override
    public double area() {
        return   Pi * side1  * side2;


    }

    /**
     * ссылка на периметр
     * @return формула перимитра для фигуры
     */
    @Override
    public double perimeter() {
        return   4 * ((Pi * side1 * side2) + (side1 - side2)) / (side1 + side2);
       // System.out.println("Пириметр Эллипса =" + perimeter);

    }
}
