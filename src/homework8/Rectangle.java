package homework8;

public class Rectangle extends Shapes{

    public Rectangle(int side1, int side2 ) {
        super(0, 0, side1, side2);
    }

    @Override
    public double area() {
        return side1 *side2;
       // System.out.println("Площадь прямоугольника =" + area);
    }

    @Override
    public double perimeter() {
       return  2 * (side1 + side2);
       // System.out.println("Прериметр прямоугольника =" + perimeter);
    }
}
