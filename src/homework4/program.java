package homework4;

public class program {
    public static void main(String[] args) {
        int[] arr = {2, 3, 4, 10, 40, 90, 100, 1000, 2000, 3333};
        int result = binarySearch(arr, 0, arr.length - 1, 3333);


        if (result == -1)
            System.out.println("Element not present");
        else
            System.out.println("Element found at index" + result);
    }

    private static int binarySearch(int arr[], int i, int y, int x) {
        if(y >= i & i < arr.length - 1 ){
            int mid = i + (y - 1) / 2;

            if (arr[mid] == x)
                return mid;

            if(arr[mid] > x)
                return binarySearch(arr, mid + 1, y, x );
        }
        return  -1;
    }

}
