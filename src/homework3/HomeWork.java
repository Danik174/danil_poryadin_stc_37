package homework3;

import java.util.Scanner;
import java.util.Arrays;

class HomeWork {
    public static int getSumOfMassive() {

        int array[] = inputArray();

        int arraySum = 0;
        for (int i = 0; i < array.length; i++) {
            arraySum += array[i];
        }

        return arraySum;
    }

    public static int[] expendMassive() {

        int[] array = inputArray();
        int box = 0;

        for (int i = 0; i < array.length / 2; i++) {
            box = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = box;
        }
        return array;
    }

    public static int[] inputArray() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите количество массивов:" + " ");
        int n = scanner.nextInt();
        int[] array = new int[n];

        System.out.println("Введите значения массивов:");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    public static double arrayAverge() {
        int[] array = inputArray();
        double arrayAverge = 0;
        if (array.length > 0) {

            double sum = 0;

            for (int i = 0; i < array.length; i++) {

                sum += array[i];
                arrayAverge = sum / array.length;
            }
        }
        return arrayAverge;
    }

    public static int[] changePosition() {
        int[] array = inputArray();

        System.out.println(Arrays.toString(array));
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int numbers[] = new int[n];
        int temp;

        int changePosition;

        for (int i = 0; i < array.length; i++) {

        }
        int max = 0;
        int min = 0;
        for (int i = 0; i < array.length; i++) {

            if (array[i] < array[min]) {
                min = i;
            }
            if (array[i] >= array[max]) {
                max = i;
            }

            temp = array[min];
            array[min] = array[max];
            array[max] = temp;


        }
        return array;
    }

    public static int[] sort(int[] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[j] < array[i]) {
                    int aj = array[j];
                    int ai = array[i];
                    array[j] = ai;
                    array[i] = aj;
                }
            }
        }
        return array;
    }


        public static void main (String[]args){

            int result = getSumOfMassive();
            System.out.println("Сумма массивов ровна:" + result);

            int[] result2 = expendMassive();
            System.out.print("Зеркальный вид массивов:");
            System.out.println(Arrays.toString(result2));

            double result3 = arrayAverge();
            System.out.println("Среднее арифметическое равно:" + result3);

            int[] result4 = changePosition();
            System.out.print("Замена положения максимального и минимального числа:");
            System.out.println(Arrays.toString(result4));
        }
    }






