package homework2;

import java.util.Scanner;

class HomeWork4{
	public static void main(String[] args) {
		System.out.println("Enter numbers:");
		Scanner scanner = new Scanner(System.in);
		int currentNumber = scanner.nextInt();

		int simpleCount = 0;
		int compositeCount = 0;
		int digitsSum = 0;

		while(currentNumber != 0 ){
		    digitsSum +=(currentNumber % 10);
			currentNumber /= 10;
			currentNumber++;
			currentNumber = scanner.nextInt();

		}
		System.out.println(digitsSum + " ");
		//System.out.println(digitsSum);
	}
}			