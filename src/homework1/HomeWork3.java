package homework1;

import java.util.Scanner;

class HomeWork3{
	public static void main(String[] args) {
		System.out.println("Enter numbers:");
		Scanner scanner = new Scanner(System.in);
		int total = 1;
		int currentNumber = scanner.nextInt();

		while(currentNumber != 0 ){
			int candidateNumber = currentNumber;
			int digitsSum = 0;
			for(int i = 100000; i >= 1; i /= 10){
				int simpleCount = candidateNumber /i;
				if(simpleCount != 0){
					digitsSum += simpleCount;
					candidateNumber -= simpleCount * i;
				}
			}
			boolean isPrime = true;
			for(int i = 2;i< digitsSum / 2;i++){
				if(digitsSum % i == 0) isPrime = false;
				break;
			}
			if(isPrime) total *= currentNumber;
			currentNumber = scanner.nextInt();	
		}
		System.out.println(total);
	}
}